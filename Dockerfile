FROM certbot/certbot

RUN pip install certbot-dns-hetzner

COPY entrypoint.sh .

ENTRYPOINT ["./entrypoint.sh"]
