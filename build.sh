#!/bin/bash

set -e

REPO=registry.gitlab.com/tlux/certbot-hetzner

docker build -t ${REPO} .
docker push ${REPO}

docker buildx build \
  --push \
  -t "${REPO}:arm32v7-latest" \
  --platform linux/arm/v7 \
  -f Dockerfile.arm \
  .