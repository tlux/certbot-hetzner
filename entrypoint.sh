#!/bin/sh

set -e

CREDENTIALS_PATH=/opt/certbot/hetzner-credentials.ini

echo "dns_hetzner_api_token = ${HETZNER_API_TOKEN}" > ${CREDENTIALS_PATH}
chmod 600 ${CREDENTIALS_PATH}

certbot certonly \
  --authenticator dns-hetzner \
  --dns-hetzner-credentials ${CREDENTIALS_PATH} \
  --non-interactive \
  --email "${CERTBOT_EMAIL}" \
  --agree-tos \
  $@

rm ${CREDENTIALS_PATH}
