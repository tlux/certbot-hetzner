# certbot-hetzner

Issues certificates via dns-01 challenge utilizing the Hetzner DNS API.

This is just Docker image wrapping the brilliantly helpful
[certbot-dns-hetzner](https://github.com/ctrlaltcoop/certbot-dns-hetzner)
plugin.

## Usage

Pull the image:

```sh
docker pull registry.gitlab.com/tlux/certbot-hetzner
```

You will need a [Hetzner DNS API
Token](https://dns.hetzner.com/settings/api-token) so the plugin can create TXT
records that are needed to verify you are the owner of the domain.

When ready, run this:

```sh
docker run \
  -it \
  -e CERTBOT_EMAIL=your@mail.com \
  -e HETZNER_API_TOKEN=s3cret \
  --rm \
  -v "/path/to/your/certs:/etc/letsencrypt" \
  registry.gitlab.com/tlux/certbot-hetzner \
  -d yourdomain.com \
  -d *.yourdomain.com \
  -d anotherdomain.net
```

Additional arguments are passed to the `certbot certonly` command in the container.
So you can do dry runs as well:

```sh
docker run \
  ... \
  --dry-run
```

## Development

When updating the Dockerfile or entrypoint.sh, update the image in the container
registry:

```sh
./build.sh
```

The build script utilizes buildx to cross-build for amd64 and arm (e.g.
Raspberry) architectures. If having trouble building, ensure you have proper
emulators installed:

```sh
docker run --privileged --rm tonistiigi/binfmt --install all
```
